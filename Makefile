# Makefile for GFALIST (c) Peter Backes
# Last modified by Markus Hoffmann 2014,2016

# This file is part of GFALIST, the GFA-BASIC detonenizer
# =======================================================
# GFALIST is free software and comes with NO WARRANTY - 
# read the file COPYING for details.

# Insert the defs for your machine

SHELL=/bin/sh

DISTRIB = ons
LIBNO=0.01
RELEASE=1
NAME=gfalist

# Directories
prefix=/usr
exec_prefix=${prefix}
datarootdir=${prefix}/share

BINDIR=${exec_prefix}/bin
LIBDIR=${exec_prefix}/lib
DATADIR=${datarootdir}
MANDIR=${datarootdir}/man
INCDIR=${prefix}/include/multiskin

# Register variables (-ffixed-reg)
REGS= -fno-omit-frame-pointer
# Optimization and debugging options
OPT=-O3

# Additional header file paths
INC= -I.

# Compiler
#CC=gcc  $(REGS)
CC=gcc -Wall
CCDEBUG=gcc -g 
CCSHARED=gcc -fPIC -shared -Wl,-Bsymbolic-functions

# Cross-Compiler fuer Windows-Excecutable
WINCC=i686-w64-mingw32-gcc

# Cross-Compiler fuer ATARI ST TOS
TOSCC=m68k-atari-mint-gcc

# Cross-Compiler fuer ARM-Linux-Excecutable
ARMCC=arm-linux-gcc

# Preprocessor
CPP=gcc -E

CFLAGS= $(INC) $(DEF) $(OPT) $(REGS)

# Name of the excecutable
EXE=	$(NAME)
WINEXE= $(NAME).exe
TOSEXE= $(NAME).ttp

# Headerfiles which should be added to the distribution
HSRC=	charset.h  sky.h  tables.h

TARGETS = libsky.a $(EXE)

SKY_ARC = libsky.a(sky.o) libsky.a(tables.o)
SKY_OBJS = sky.o tables.o

GFALIST_OBJS = gfalist.o charset.o

OBJS = $(SKY_OBJS) $(GFALIST_OBJS)

CSRC=	$(OBJS:.o=.c) 

MAN=	gfalist.1

DOC=	README.md COPYING HISTORY 

# Documentation files to be packed into the .deb file:
DEBDOC= copyright changelog.Debian $(DOC)
WINDOC= 
DIST=	$(HSRC) $(CSRC) $(DOC) $(MAN) Makefile
BINDIST= COPYING $(MAN) $(DOC) $(EXE)

WINDIST= $(WINEXE) $(WINDOC) \
	 COPYING HISTORY 

all: version.h $(TARGETS)

libsky.a: $(SKY_OBJS)
	$(AR) rcv $@ $?
	ranlib $@

# Updating on a per file basis: 
#libsky.a: $(SKY_ARCH) $(SKY_OBJS)
#	ranlib $@
#(%.o): %.o
#	$(AR) rcv $@ $<

# Updating with intermediate files:
#libsky.a: $(SKY_ARCH)
#	ranlib $@
#(%.o): %.c
#	$(CC) $(CFLAGS) -c $< -o $*.o
#	$(AR) rcv $@ $*.o
#	rm $*.o

gfalist: $(GFALIST_OBJS) libsky.a $(HSRC) version.h
	$(CC) -g $(LFLAGS) $(GFALIST_OBJS) libsky.a -o $@
gfalist.ttp: $(CSRC) $(HSRC) version.h
	$(TOSCC) $(CSRC) -o $@ 

version.h: HISTORY sh/verextr.sh $(SKY_OBJS)
	sh sh/verextr.sh -g $< $@

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f $(OBJS) $(TRASH)

realclean: clean
	rm -f $(TARGETS) gfalist.exe gfalist.ttp gfalist_$(LIBNO)-$(RELEASE)_*.deb


pdist: MANIFEST HISTORY sh/packdist.sh
	sh sh/packdist.sh -t $(DISTRIB) -m $< -v HISTORY,version.h -s ons.spec ck md

# Make the exe for MS WINDOWS
$(WINEXE): $(CSRC) $(HSRC) version.h Makefile 
	$(WINCC) $(OPT) -DWINDOWS -Wall -o $@ $(CSRC)

install: $(NAME) $(NAME).1
	install -d $(BINDIR)
	install -s -m 755 $(NAME) $(BINDIR)/
	install -d $(MANDIR)
	install -d $(MANDIR)/man1
	install -m 644 $(NAME).1 $(MANDIR)/man1/
uninstall :
	rm -f $(BINDIR)/$(NAME)
	rm -f $(MANDIR)/man1/$(NAME).1
	rmdir --ignore-fail-on-non-empty $(BINDIR)
	rmdir --ignore-fail-on-non-empty $(MANDIR)/man1
	rmdir --ignore-fail-on-non-empty $(MANDIR)

doc-pak: $(DEBDOC)
	mkdir -p $@
	cp $+ $@/
	gzip -n -9 $@/changelog.Debian

# Make a tar ball with the sources  

dist :	$(NAME)-$(LIBNO).tar.gz
$(NAME)-$(LIBNO).tar.gz : $(DIST)
	rm -rf /tmp/$(NAME)-$(LIBNO)
	mkdir /tmp/$(NAME)-$(LIBNO)
	(tar cf - $(DIST))|(cd /tmp/$(NAME)-$(LIBNO); tar xpf -)
	(cd /tmp; tar cf - $(NAME)-$(LIBNO)|gzip -9 > $@)
	mv /tmp/$@ .

# Make a simple debian package which can easily be installed and removed 
# from the system

deb :	$(BINDIST) doc-pak
	sudo checkinstall -D --pkgname $(NAME) --pkgversion $(LIBNO) \
	--pkgrelease $(RELEASE)  \
	--maintainer kollo@users.sourceforge.net \
	--requires libc6 --backup \
	--pkggroup interpreters \
	--pkglicense GPL --strip=yes --stripso=yes --reset-uids
	rm -rf backup-*.tgz doc-pak

rpms: pdist
	sh sh/packdist.sh -t $(DISTRIB) -v HISTORY,version.h mr

ons.spec: README.md HISTORY sh/packdist.sh
	sh sh/packdist.sh -a README.md -v HISTORY,version.h -t $(DISTRIB) -s ons.spec fs

distclean: clean
	rm -f $(EXE) $(WINEXE) $(TOSEXE)
	rm -rf doc-pak

#DEPEND
gfalist: libsky.a
sky.o: sky.c sky.h tables.h
gfalist.o: gfalist.c $(HSRC) version.h

